require('dotenv').config();

var app = require('koa')();
var router = require('koa-router')();
var cors = require('koa-cors');
var bodyParser = require('koa-bodyparser');

var Song = require('./models/song');
var Artist = require('./models/artist');
var SongsCollection = require('./collections/song');

router.use(cors());
router.use(bodyParser());

router.get('/api/songs', function*() {
  var songs = yield Song.fetchAll();
  this.body = songs;
});

router.get('/api/songs/:id', function*() {
  try {
    let song = yield Song.where('id', this.params.id).fetch({ 
      require: true,
      withRelated: ['artist']
    });

    this.body = song;
  } catch(e) {
    this.status = 404;
    this.body = {
      error: {
        message: 'Song not found'
      }
    };
  }
});

router.post('/api/artists', function*() {
  try {
    var artist = new Artist({
      artist_name: this.request.body.name
    });

    yield artist.save();
    this.body = artist;
  } catch(e) {
    this.body = {
      error: 'something went wrong'
    };
  }
});

router.put('/api/artists/:id', function*() {
  try {
    var artist = yield Artist.where('id', this.params.id).fetch({ require: true });
    artist.set('artist_name', this.request.body.name);
    yield artist.save();
    this.body = artist;
  } catch(e) {
    this.status = 404;
    this.body = {
      error: {
        message: 'artist not found'
      }
    };
  }
});

app
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(process.env.PORT || 3000);

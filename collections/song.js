var bookshelf = require('./../bookshelf');
var Song = require('./../models/song');

module.exports = bookshelf.Collection.extend({
  model: Song
});